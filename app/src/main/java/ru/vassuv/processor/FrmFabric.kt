// """
// |              ********************************
// |              *      Генерируемый файл       *
// |              ********************************
// |              *                              *
// |              *                              *
// |              *                              *
// |              *                              *
// |              *                              *
// |              *                              *
// |              *                              *
// |              *                              *
// |              *                              *
// |              ********************************
// |""".trimMargin()
package ru.vassuv.processor

import android.os.Bundle
import android.support.v4.app.Fragment
import com.example.root.annotationtest.fragment.BlankFragment
import com.example.root.annotationtest.fragment.ListFragment
import com.example.root.annotationtest.fragment.PlusOneFragment
import kotlin.String
import kotlin.jvm.JvmStatic

object FrmFabric {
    val BLANK: String = "BLANK" // BlankFragment

    val LIST: String = "LIST" // ListFragment

    val PLUS_ONE: String = "PLUS_ONE" // PlusOneFragment

    @JvmStatic
    private fun createBlankFragment(args: Bundle? = null): BlankFragment {
        val fragment = BlankFragment()
        fragment.arguments = args
        return fragment
    }

    @JvmStatic
    private fun createListFragment(args: Bundle? = null): ListFragment {
        val fragment = ListFragment()
        fragment.arguments = args
        return fragment
    }

    @JvmStatic
    private fun createPlusOneFragment(args: Bundle? = null): PlusOneFragment {
        val fragment = PlusOneFragment()
        fragment.arguments = args
        return fragment
    }

    @JvmStatic
    fun createFragment(fragmentName: String, args: Bundle? = null): Fragment {
        val fragment: Fragment = when(fragmentName) {
            BLANK -> BlankFragment()
            LIST -> ListFragment()
            PLUS_ONE -> PlusOneFragment()
            else -> throw Exception("Передан не аннатируемый фрагмент")
        }
        fragment.arguments = args
        return fragment
    }

    @JvmStatic
    fun valueOf(fragment: Fragment): String {
        val name: String = when(fragment::class) {
            BlankFragment::class -> BLANK
            ListFragment::class -> LIST
            PlusOneFragment::class -> PLUS_ONE
            else -> ""
        }
        return name
    }
}
