package com.example.root.annotationtest.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.root.annotationtest.adapter.MyItemRecyclerViewAdapter
import com.example.root.annotationtest.R
import com.example.root.annotationtest.dummy.DummyContent
import com.example.root.annotationtest.dummy.DummyContent.DummyItem
import kotlinx.android.synthetic.main.fragment_item_list.*
import ru.vassuv.processor.FrmFabric
import ru.vassuv.processor.annotation.Route

@Route
class ListFragment : Fragment() {

    private var columnCount = 1

    private val listener = object : OnListFragmentInteractionListener {
        override fun onListFragmentInteraction(item: DummyItem?) {
            item ?: return
            fragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainer, FrmFabric.createFragment(item.content))
                    .addToBackStack(null)
                    .commit()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_item_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list.adapter = MyItemRecyclerViewAdapter(DummyContent.ITEMS, listener)
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: DummyItem?)
    }

    companion object {
        const val ARG_COLUMN_COUNT = "column-count"
    }
}
