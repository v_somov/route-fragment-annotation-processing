package com.example.root.annotationtest.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.root.annotationtest.R
import kotlinx.android.synthetic.main.fragment_plus_one.*
import ru.vassuv.processor.annotation.Route

@Route
class PlusOneFragment : Fragment() {
    private val PLUS_ONE_URL = "http://developer.android.com"

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_plus_one, container, false)
    }

    override fun onResume() {
        super.onResume()

        plus_one_button.initialize(PLUS_ONE_URL, PLUS_ONE_REQUEST_CODE)
    }

    companion object {
        private val PLUS_ONE_REQUEST_CODE = 0
    }

}
