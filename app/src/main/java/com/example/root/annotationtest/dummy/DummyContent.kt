package com.example.root.annotationtest.dummy

import ru.vassuv.processor.FrmFabric
import java.util.ArrayList
import java.util.HashMap

object DummyContent {

    val ITEMS: MutableList<DummyItem> = ArrayList()

    val ITEM_MAP: MutableMap<Int, DummyItem> = HashMap()

    init {
        addItem(DummyItem(1, FrmFabric.LIST, "Список Экранов"))
        addItem(DummyItem(1, FrmFabric.BLANK, "Бланк"))
        addItem(DummyItem(1, FrmFabric.PLUS_ONE, "Плюс 1"))
    }

    private fun addItem(item: DummyItem) {
        ITEMS.add(item)
        ITEM_MAP[item.id] = item
    }

    data class DummyItem(val id: Int, val content: String, val details: String) {
        override fun toString(): String = content
    }
}
