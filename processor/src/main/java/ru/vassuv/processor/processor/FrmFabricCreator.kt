package ru.vassuv.processor.processor

import com.squareup.kotlinpoet.*
import ru.vassuv.processor.annotation.Route
import javax.lang.model.element.Element
import javax.lang.model.util.Elements

class FrmFabricCreator(elementUtils: Elements?,
                       val annotatedElements: MutableSet<out Element>,
                       val packageName: String,
                       val fileName: String) {

    private val bundleTypeName: TypeName
    private val supportFragmentTypeName: TypeName
    private var bundleArgsParameter: ParameterSpec

    init {
        elementUtils ?: throw Exception("ElementUtils not found")

        bundleTypeName = elementUtils.getTypeElement("android.os.Bundle")
                ?.asType()
                ?.asTypeName()
                ?.asNullable()
                ?: throw Exception("Bundle TypeName not init")

        supportFragmentTypeName = elementUtils.getTypeElement("android.support.v4.app.Fragment")
                ?.asType()
                ?.asTypeName()
                ?: throw Exception("Fragment TypeName not init")

        bundleArgsParameter = ParameterSpec.builder("args", bundleTypeName).defaultValue("null").build()
    }

    fun generate() = FileSpec.builder(packageName, fileName)
            .addComment("%S", getCommentFile())
            .addType(generateClass(annotatedElements))
            .build()

    private fun generateClass(annotatedElements: MutableSet<out Element>): TypeSpec {
        var fragmentTypeName: TypeName
        var className: String
        var getterName: String
        var propName: String

        val frmFabricClass = TypeSpec.objectBuilder(fileName)
        val listSwitcherStatementCreateFragment = arrayListOf<String>()
        val listSwitcherStatementValueOf = arrayListOf<String>()

        annotatedElements.forEach { element ->
            className = element.simpleName.toString()
            getterName = "create$className"
            propName = element.getAnnotation(Route::class.java).name
            propName = if (propName.isEmpty()) formatPropName(className) else propName
            fragmentTypeName = element.asType().asTypeName()

            frmFabricClass
                    .addFunction(createFunctionSpec(getterName, bundleArgsParameter, fragmentTypeName, element)) // <- 1
                    .addProperty(createValueSpec(propName, className))                                      // <- 2

            listSwitcherStatementCreateFragment.add("    $propName -> ${element.simpleName}()")
            listSwitcherStatementValueOf.add("    $className::class -> $propName")
        }

        frmFabricClass.addFunction(getCreateFunctionSpec(listSwitcherStatementCreateFragment))                   // <- 3
        frmFabricClass.addFunction(getValueOfFunctionSpec(listSwitcherStatementValueOf))                         // <- 4

        return frmFabricClass.build()
    }

    private fun formatPropName(classFragmentName: String) = classFragmentName
            .replace("Fragment$".toRegex(), "")
            .replace("([a-z]+)".toRegex(), { matchResult -> matchResult.groupValues[0].toUpperCase() + "_" })
            .trimEnd('_')

    private fun createFunctionSpec(getterName: String,
                                   bundleArgsParametr: ParameterSpec,
                                   fragmentTypeName: TypeName,
                                   element: Element
    ) = FunSpec.builder(getterName)
            .addAnnotation(JvmStatic::class)
            .addParameter(bundleArgsParametr)
            .returns(fragmentTypeName)
            .addStatement("val fragment = ${element.simpleName}()")
            .addStatement("fragment.arguments = args")
            .addStatement("return fragment")
            .addModifiers(KModifier.PRIVATE)
            .build()

    private fun createValueSpec(propName: String, className: String) = PropertySpec
            .builder(
                    propName,
                    String::class.asTypeName(),
                    KModifier.PUBLIC)
            .initializer("\"$propName\" // $className")
            .build()

    private fun getCreateFunctionSpec(listSwitcherStatementCreateFragment: ArrayList<String>): FunSpec {
        return FunSpec.builder("createFragment").apply {
            addAnnotation(JvmStatic::class)
            addParameter("fragmentName", String::class)
            addParameter(bundleArgsParameter)
            returns(supportFragmentTypeName)
            addStatement("val fragment: Fragment = when(fragmentName) {")
            listSwitcherStatementCreateFragment.forEach { statement ->
                addStatement(statement)
            }
            addStatement("    else -> throw Exception(\"Передан не аннатируемый фрагмент\")")
            addStatement("}")
            addStatement("fragment.arguments = args")
            addStatement("return fragment")
        }.build()
    }

    private fun getValueOfFunctionSpec(listSwitcherStatementValueOf: ArrayList<String>): FunSpec {
        return FunSpec.builder("valueOf").apply {
            addAnnotation(JvmStatic::class)
            addParameter("fragment", supportFragmentTypeName)
            returns(String::class)
            addStatement("val name: String = when(fragment::class) {")
            listSwitcherStatementValueOf.forEach { statement ->
                addStatement(statement)
            }
            addStatement("    else -> \"\"")
            addStatement("}")
            addStatement("return name")
        }.build()
    }

    private fun getCommentFile() =
            "              ********************************\n" +
                    "              *      Генерируемый файл       *\n" +
                    "              ********************************\n" +
                    "              *                              *\n" +
                    "              *                              *\n" +
                    "              *                              *\n" +
                    "              *                              *\n" +
                    "              *                              *\n" +
                    "              *                              *\n" +
                    "              *                              *\n" +
                    "              *                              *\n" +
                    "              *                              *\n" +
                    "              ********************************\n"
}