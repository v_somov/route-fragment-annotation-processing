package ru.vassuv.processor.processor

import com.google.auto.service.AutoService
import com.squareup.kotlinpoet.*
import ru.vassuv.processor.annotation.Route
import java.io.File
import java.io.IOException
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.Processor
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.TypeElement
import javax.lang.model.util.Elements
import javax.lang.model.util.Types
import javax.tools.Diagnostic


@AutoService(Processor::class)
class RouteProcessor : AbstractProcessor() {
    private val FILE_NAME = "FrmFabric"
    private val PACKAGE = "ru.vassuv.processor"
    private var projectDir: String = ""
    private var elementsUtils: Elements? = null

    override fun getSupportedAnnotationTypes() = linkedSetOf(Route::class.java.canonicalName)

    override fun getSupportedSourceVersion() = SourceVersion.latestSupported()

    override fun init(processingEnvironment: ProcessingEnvironment?) {
        projectDir = getProjectDir(processingEnvironment)
        elementsUtils = processingEnvironment?.elementUtils
    }

    override fun process(annotations: MutableSet<out TypeElement>,
                         roundEnv: RoundEnvironment): Boolean {
        val annotatedElements = getRouteElements(roundEnv, annotations) ?: return false
        val file: FileSpec = FrmFabricCreator(elementsUtils, annotatedElements, PACKAGE, FILE_NAME).generate()

        file.writeTo(File(projectDir, ""))
        return true
    }

    private fun getProjectDir(processingEnvironment: ProcessingEnvironment?) = processingEnvironment?.let {
        try {
            val generationForPath = processingEnvironment.filer.createSourceFile("PathFor" + javaClass.simpleName)
            val writer = generationForPath.openWriter()
            val sourcePath = generationForPath.toUri().path.substringBeforeLast("build/generated")
            writer.close()
            generationForPath.delete()
            sourcePath + "src/main/java"
        } catch (e: IOException) {
            processingEnvironment.messager.printMessage(Diagnostic.Kind.WARNING, "Unable to determine source file path!")
            "" // "/home/developer/git/AnnotationTest/app/src/main/java/"
        }
    } ?: ""

    private fun getRouteElements(roundEnv: RoundEnvironment, annotations: MutableSet<out TypeElement>): MutableSet<out Element>? {
        return roundEnv.getElementsAnnotatedWith(annotations.find { it.simpleName.toString() == "Route" }
                ?: return null)
    }
}
